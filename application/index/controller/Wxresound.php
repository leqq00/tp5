<?php

namespace app\index\controller;

use think\Controller;
use think\Request;
use Wxpay\WxPayConfig;

class Wxresound extends Controller
{
    //微信退款
    public function index(Request $request)
    {
        require_once EXTEND_PATH . 'Wxpay/lib/WxPay.Api.php';
        $input = new \WxPayRefund();
        // 设置商户系统内部的订单号, transaction_id、out_trade_no二选一，如果同时存在优先级：transaction_id > out_trade_no
        $input->SetOut_trade_no('zlwxpay009');
        //微信订单号
        //        $input->SetTransaction_id('4200000244201902187814048274');
        //设置订单总金额，单位为分，只能为整数，详见支付金额
        $input->SetTotal_fee(1);
        // 设置退款总金额，订单总金额，单位为分，只能为整数
        $input->SetRefund_fee(1);
        $config = new WxPayConfig();
        //设置商户系统内部的退款单号，商户系统内部唯一，同一退款单号多次请求只退一笔
        $input->SetOut_refund_no('tzlwxpay007' . time());
        //设置操作员帐号, 默认为商户号
        $input->SetOp_user_id($config->GetMerchantId());
        $rearr = \WxPayApi::refund($config, $input);
        dump($rearr);
        if (isset($rearr['err_code_des'])) {
            //退款异常
            die($rearr['err_code_des']);
        }
    }
}
