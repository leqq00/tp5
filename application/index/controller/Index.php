<?php

namespace app\index\controller;

use Firebase\JWT\JWT;
use think\Controller;

class Index extends Controller
{
    public function index()
    {
        //用来解密加密验证的key
        $key = md5('test');
        //签发的token
        $token = [
            'iat' => time(),    //签发时间
            'exp' => time() + 7200,  //过期时间
            'uid' => 1
        ];
        $jwt = JWT::encode($token, $key);
        $info = JWT::decode($jwt, $key, ["HS256"]);
        $this->assign('jwt', $jwt);
        $this->assign('info', $info);
        return $this->fetch('index');
    }
}
