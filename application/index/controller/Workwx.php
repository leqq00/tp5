<?php

namespace app\index\controller;


use think\Cache;
use think\Controller;

class Workwx extends Controller
{

    public $corpid = 'wx864d82e8d00e8ae7';
    //通讯录
    public $corp = 'crYDf8WL0Y68AzM7XmOoLDAQXWpd_dUduDb3xi_x6SA';

    /**
 * 删除方法
 *
 * @return void
 */
    public function deluser()
    {
        $token = $this->getaccesstoken($this->corp);
        $userid = 'bailin';
        if (is_array($userid)) {
            $url = "https://qyapi.weixin.qq.com/cgi-bin/user/batchdelete?access_token=$token";
            $data = json_encode($userid);
            $repose = json_decode($this->curl($url, $data));
        } else {
            $url = "https://qyapi.weixin.qq.com/cgi-bin/user/delete?access_token=$token&userid=$userid";
            $repose = json_decode($this->curl($url));
        }
        dump($repose);
    }

    /**
     * 发送消息
     */
    public function senmessage()
    {
        //自己创建的应用
        $token = $this->getaccesstoken('x_yJBvB53Dh3MYpKAr-M3l1oo9u68s6uJ-BwNmMdjgA');
        $url = "https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=$token";
        $messinfo = [
            'touser' => 'huangwei|panpan|zhuchuanwang|zengling',
            'msgtype' => 'textcard',
            //应用id
            'agentid' => '0',
            'textcard' => [
                'title' => '恭喜你中奖了,牛逼大奖',
                'description' => '哈哈,请给曾帅哥转账10元,现金也可以',
                'url' => 'www.baidu.com',
                'btntxt' => '查看详情'
            ]
        ];
        $jsmessage = json_encode($messinfo, JSON_UNESCAPED_UNICODE);
        $repose = json_decode($this->curl($url, $jsmessage));
        if ($repose->errcode == 0) {
            return $repose->errmsg;
        } else {
            dump($repose);
            die;
        }
    }


    /**
     * @return mixed
     * 创建用户
     */
    public function createuser()
    {
        $token = $this->getaccesstoken($this->corp);
        $update = "https://qyapi.weixin.qq.com/cgi-bin/user/update?access_token=$token";
        $url = "https://qyapi.weixin.qq.com/cgi-bin/user/create?access_token=$token";
        $userinfo = [
            'userid' => 'wangsong',
            'name' => '王松',
            'alias' => '松松',
            'mobile' => '13579246810', //13882013290
            'department' => '1',
            'gender' => '2',
            'position' => '开发'
        ];
        $jsuser = json_encode($userinfo, JSON_UNESCAPED_UNICODE);
        $repose = json_decode($this->curl($update, $jsuser));
        if ($repose->errcode == 0) {
            return $repose->errmsg;
        } else {
            //60104 手机号码已存在 成员的手机号不能重复
            dump($repose);
            die;
        }
    }

    /**
     * @return mixed
     * 获取access_token
     */
    public function getaccesstoken($corp)
    {
        if (Cache::get($corp)) {
            $token = Cache::get($corp);
            return $token;
        }
        $cid = $this->corpid;
        $url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=$cid&corpsecret=$corp";
        $repose = json_decode($this->curl($url));
        if ($repose->errcode == 0) {
            $access_token = $repose->access_token;
            $exp = ($repose->expires_in) - 100;
            Cache::set($corp, $access_token, $exp);
            return $access_token;
        }
    }

    public function curl($url, $data = false, $ssl = true)
    {
        // curl完成
        $curl = curl_init();
        //设置curl选项
        curl_setopt($curl, CURLOPT_URL, $url);
        $user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '
Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0 FirePHP/0.7.4';
        curl_setopt($curl, CURLOPT_USERAGENT, $user_agent); //user_agent，请求代理信息
        curl_setopt($curl, CURLOPT_AUTOREFERER, true); //referer头，请求来源
        //SSL相关
        if ($ssl) {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); //禁用后cURL将终止从服务端进行验证
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2); //检查服务器SSL证书中是否存在一个公用名(common name)。
        }

        // 处理post相关选项
        if ($data) {
            curl_setopt($curl, CURLOPT_POST, true); // 是否为POST请求
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data); // 处理请求数据
        }
        // 处理响应结果
        curl_setopt($curl, CURLOPT_HEADER, false); //是否处理响应头
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); //curl_exec()是否返回响应结果
        // 发出请求
        $response = curl_exec($curl);
        if (false === $response) {
            echo '<br>', curl_error($curl), '<br>';
            return false;
        }
        return $response;
    }
}
