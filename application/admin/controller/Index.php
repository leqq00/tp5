<?php

namespace app\admin\controller;

use think\Controller;
use think\Db;
use think\Request;
use think\Session;

class Index extends Controller
{
    /**
     * @return mixed
     * 后台登陆界面
     */
    public function Index()
    {
        // 临时关闭当前模板的布局功能
        $this -> view -> engine -> layout(false);
        return $this -> fetch('login/login');
    }

    /**
     * @param Request $request
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * 登陆逻辑
     */
    public function logindo(Request $request)
    {
        $username = $request -> param('username');
        $password = ($request -> param('password'));
//        $result = Db::table('')->where('', $username)->where('', $password)->find();
        if ($username = 'admin' && $password = 'admin') {
            Session ::set('adminer', $username);
            return $this -> redirect('admin/admin/admin');
        }
        return $this -> error('用户名或者密码错误', 'admin/index/index');

    }

    /**
     * 退出登录
     */
    public function loginout()
    {
        Session ::delete('adminer');
        return $this -> success('退出登录成功', 'admin/index/index', '', 2);
    }
}