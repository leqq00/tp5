<?php
/**
 * Created by PhpStorm.
 * User: 10310
 * Date: 2018/5/22
 * Time: 19:43
 */

namespace app\admin\controller;


use think\Controller;
use think\Session;

class Basecontroller extends Controller
{
    /**
     * 前置登陆检测
     */
    public function isLogin()
    {
        if (!(Session::get('adminer'))) {
            return $this->error('请登陆后操作', 'admin/index/index', '', 2);
        }
    }
}