<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/23
 * Time: 11:47
 */

namespace app\admin\controller;

use app\common\Mail;
use think\Request;

class Mails extends Basecontroller
{
    /**
     * @return mixed
     * 邮箱设置界面
     */
    public function index()
    {
       
        return $this -> fetch('setmail');
    }

    /**
     * @throws \PHPMailer\PHPMailer\Exception
     * 发送邮件
     */
    public function sendmail()
    {
        $mail = new Mail();
        $toemail = '1179979198@qq.com';
        $name = 'xule';
        $title = 'QQ邮件发送测试';
        $content = '恭喜你，邮件测试成功。';
        $res = $mail -> send_mail($toemail, $name, $title, $content);
        if ($res == true) {
            return json(['message' => '发送成功']);
        }
        return json(['message' => $res]);
    }

}