<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/22
 * Time: 8:47
 */

namespace app\common;


use think\Config;
use think\Controller;
use PHPMailer\PHPMailer\PHPMailer;

class Mail extends Controller
{
    /**
     * 邮件发送方法
     * @param        $tomail 收件人邮箱
     * @param        $name  收件人名字
     * @param string $title 邮件标题
     * @param string $body 邮件内容
     * @param null   $attachment 是否添加附件   数组格式
     * @return bool|string 返回成功或者失败信息
     * @throws \PHPMailer\PHPMailer\Exception
     */
    function send_mail($tomail, $name, $title = '', $body = '', $attachment = null)
    {
        $mail = new PHPMailer();           //实例化PHPMailer对象
        $mail -> CharSet = 'UTF-8';           //设定邮件编码，默认ISO-8859-1，如果发中文此项必须设置，否则乱码
        $mail -> IsSMTP();                    // 设定使用SMTP服务
        $mail -> SMTPDebug = 0;               // SMTP调试功能 0=关闭 1 = 错误和消息 2 = 消息
        $mail -> SMTPAuth = true;             // 启用 SMTP 验证功能
        $mail -> SMTPSecure = 'ssl';          // 使用安全协议
        $mail -> Host = Config ::get('mails.host'); // SMTP 服务器 邮件服务器
        $mail -> Port = Config ::get('mails.port');                  // SMTP服务器的端口号
        $mail -> Username = Config ::get('mails.username');    // SMTP服务器用户名 邮箱账户名
        $mail -> Password = Config ::get('mails.password');     // SMTP服务器密码 邮箱账户密码或者是邮箱授权码
        $mail -> SetFrom(Config ::get('mails.username'), Config ::get('mails.nickname'));//设置发件人信息 发件人邮箱和名字
        $replyEmail = '';                   //留空则为发件人EMAIL
        $replyName = '';                    //回复名称（留空则为发件人名称）
        $mail -> AddReplyTo($replyEmail, $replyName);// 设置回复人信息，指的是收件人收到邮件后，如果要回复，回复邮件将发送到的邮箱地址
        $mail -> Subject = $title;//发送的标题
        $mail -> MsgHTML($body);//发送的主题内容
        $mail -> AddAddress($tomail, $name);// 设置收件人信息，如邮件格式说明中的收件人
        if (is_array($attachment)) { // 是否添加附件
            foreach ($attachment as $file) {
                is_file($file) && $mail -> AddAttachment($file);
            }
        }
        return $mail -> Send() ? true : $mail -> ErrorInfo;
    }

}