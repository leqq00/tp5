<?php
namespace app\api\controller;

use think\Controller;
use think\Request;

class Index extends Controller
{
    public function index(Request $re)
    {
        # 反射api
        $resultdata = ['code' => 0, 'data' => '', 'message' => ''];
        //获取当前文件所在的绝对目录
        $dir = dirname(__FILE__);
        //扫描文件夹
        $classarr = $this->getFile($dir);
        //删除自己的当前类
        $thisclassname = get_class();
        $delindex = strtolower(substr($thisclassname, strripos($thisclassname, '\\') + 1, 1));
        unset($classarr[$delindex]);
        $classme = $re->param('m');
        //指定命名空间
        $namespace = __NAMESPACE__;
        //寻找类名
        $temindex = substr($classme, 0, 1);
        //调用的方法名
        $methodname = substr($classme, 1);
        //查找数据的键
        $arrkey = array_keys($classarr);
        //如果键存在表示类存在
        if (in_array($temindex, $arrkey)) {
            $classname = '\\' . $classarr[$temindex];
            $rfc = new \ReflectionClass($namespace . $classname);
            //检测类是否可以实例化,抽象,静态不能实例化
            if ($rfc->isInstantiable()) {
                //实例化这个类
                $instance = $rfc->newInstance();
                //检查方法是否存在
                if ($rfc->hasMethod($methodname)) {
                // 返回一个ReflectionMethod 方法类
                    $resultdata['message'] = '成功';
                    $method = $rfc->getMethod($methodname);
                    $resultdata['data'] = ($method->invoke($instance, $re));
                } else {
                    $resultdata['code'] = -1;
                    $resultdata['message'] = '方法错误';
                }
            } else {
                $resultdata['code'] = -1;
                $resultdata['message'] = '非法的类';
            }
        } else {
            $resultdata['code'] = -1;
            $resultdata['message'] = '类错误错误';
        }
        return json($resultdata);
    }

    /**
     * 读取当前目录下的文件
     *返回一个关联数组 [i=>index]
     * @param [type] $dir
     * @return void
     */
    public function getFile($dir)
    {
        $fileArray = [];
        if (false != ($handle = opendir($dir))) {
            $i = 0;
            while (false !== ($file = readdir($handle))) {
            //去掉"“.”、“..”以及带“.xxx”后缀的文件
                if ($file != "." && $file != ".." && strpos($file, ".")) {
                    //数组的索引就是类名的首字母小写
                    $index = strtolower(substr($file, 0, 1));
                    $fileArray[$index] = strstr($file, substr($file, -4), true);
                    if ($i == 100) {
                        break;
                    }
                    $i++;
                }
            }
        //关闭句柄
            closedir($handle);
        }
        return $fileArray;
    }
}